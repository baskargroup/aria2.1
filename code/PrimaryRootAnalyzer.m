function [sroot,Root,DistMat, dist_cm, x, y, path, MedianR, MaximumR,LengthDistribution, Perimeter, Diameter, Volume, SurfaceArea,TRLUpper, TRLLower,skeBW,Area,TRAUpper, TRALower,RatY] = PrimaryRootAnalyzer(Root3pc,Pix2cm,fname,inputFolder2)
%finding the Primary root using longest shortest path and then outputs the length
%in Centimeters of the primary root. Since we know the coordinates of the primary
%root, we subtract it from the original image, thus leaving only secondary
%roots to be analyzed
% Developers : Hsiang Sing Naik & Nigel Lee
% Copyright : Baskar Ganapathysubramanian
% Version 1 : July 14, 2013
% Version 2: December 7, 2016 by Zaki Jubery

%% Consider the largest connected component as root in the segmented image
Root2=Root3pc;
Root = BoundaryChecker(Root2);   %clear the boundaries of the image which could cause problem
%% Identify connected components and set largest one as root
neigh_n = 8;    %checks all 8 positions for nearest neighbours (N,E,S,W,NE,NW,SE,SW)
CC = bwconncomp(Root,neigh_n); %locates all connected components
ImgSize = size(Root); %image size for ind2sub function which converts indexed value of image to x and y coordinates
CCinMat = zeros(1,CC.NumObjects); %preallocates Connected components in matrix format
a = CC.PixelIdxList;
for n = 1:CC.NumObjects
    CCinMat(n) = max(size(cell2mat(a(n))));       %converts CC values to matrix
end
Rootidx_ID = CCinMat == max(CCinMat);  %find the largest connected components
Rootidx = cell2mat(a(Rootidx_ID));         %finds the indexed value of the largest connected components
[y, x] = ind2sub(ImgSize,Rootidx);        %converts indexed value to x and y coordinates

%preallocats memory for replotting the root from x and y coordinates
[Row,Column] = size(Root);
Root = zeros(Row,Column);
%plots the connected components of the root which are in x and y coordinates
for n = 1 : max(size(y))
    Root(y(n),x(n))=1;
end
% measure Area
Area=bwarea(Root)/(Pix2cm)^2;

%% find primary root
[y,x,dist, path,DistMat,skeBW,XstClick,YstClick,Pt] = find_primary_root(Root,fname,inputFolder2);
%% Compute descriptors of the primary root
[MedianR, MaximumR, TRLUpper, TRLLower, LengthDistribution, Perimeter, Diameter, Volume, SurfaceArea,TRAUpper, TRALower,RatY] = Func_DescriptorsforPrimaryRoot(skeBW,Root,x,y,path,YstClick,Pix2cm);
%% Remove primary root from the image and Visualize secondary roots on to a graph.
spt = Pt;
spt(path,:) = []; %removes primary root points from the skeletonized image
sy = spt(:,1); %assign the y location of the remaining roots to sy
sx = spt(:,2); %assign the x location of the remaining roots to sx
%preallocate memory for secondary root
[Row,Column] = size(Root);
sroot = zeros(Row,Column);
%plots the secondary root from sx and sy
for n = 1 : max(size(sy))
    sroot(sy(n),sx(n))=1;
end
%converts pixel to Centimeters
dist_cm = dist/Pix2cm;
end
