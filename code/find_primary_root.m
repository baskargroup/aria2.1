function [y,x,dist, path_prim,DistMat,skeBW,XstClick,YstClick,Pt] = find_primary_root(Root,fname,inputFolder2)
% Take root image as input and identify the tap root
%% Find Starting point of the root system
% Find row and column locations that are non-zero
[y,~] = find(Root);  
%Find top left corner
ymin = min(y(:));
XstClick= min(find(Root(ymin(1),:)));
YstClick=ymin(1);
Root(1:YstClick,:) = 0;      %removes all points above the start of the primary root
%% Skeletonize and clean the image
skeBW = bwmorph(Root,'skel','inf');
skeBW = bwmorph(skeBW,'clean');
% prune spur segments
skeBW=bwmorph(skeBW,'spur',20);

%% convert the skeletonized image into a graph and find primary root
%step 1: identify all useful pixels (Find coordinate of nonzero pixels)
[y,x] = find(skeBW);
Pt = [y x];

%% shortest longest path from the top as primary root

    %step 2:find distance matrix between all these useful point
    % (Find index and distance matrix of 10 closet points for all the nonzero
    %  pixels)
    [IDX,D] = knnsearch(Pt,Pt,'K',10);
    % step 3:removes self counting
    D(:,1)= [];
    IDX(:,1) = [];
    % step 4:preallocates the distance matrix
    DistMat = sparse(max(size(y)));
    k = 0;
    for i=1:size(y,1)
        %find neighbors ID
        NN = find( D(i,:) < 2);
        if NN == 1
            %%change name (basically find the all the endpoints, because it has only one neighbour)
            Endpoints_ID(k+1) = IDX(i,NN); % all endpoints
            k = k+1;
        end
        NN_ID = IDX(i,NN);
        n_NN_ID = max(size(NN_ID));

        %find distance from NN_ID to i
        for j=1:n_NN_ID
            DistMat(i,NN_ID(j)) = D(i,NN(j));
            DistMat(NN_ID(j),i) = D(i,NN(j));
        end
    end
    % Step 5: Find closet starting point based on the mouse click ( or top
    % point)
    %finding closest start point to the coordinates the user has input
    %after skeletonizing,since everything has become one pixel
    startpts = [YstClick,XstClick];
    startpts = round(startpts);
    [Istart,~] = knnsearch(Pt,startpts, 'K',2);
    % Step 6: Find the shortest path from starting points to each endpoints
    %Locates all the Endpointss on the graph, and creates a shortest path from the
    %start point to each Endpoints. Using this concept, the end point of the primary
    %root can be located, by using the longest "shortest path" from the
    %starting point to the Endpoints.
    % [null] = Func_Diagnostic('Finding Longest Shortest Path...');
    max_length = max(size(Endpoints_ID));
    dist = zeros(max_length,1); %preallocate memory for shortest distance of all Endpoints (endpoints) ids
    for n = 1 : max_length
        [dist(n)]=graphshortestpath(DistMat,Istart(1),Endpoints_ID(n));
    end
    % Step 7: Find longest of the shortest paths, this is the primary root
    %ID of the longest "shortest path"
    n = find(dist == max(dist));
    % Step 8: Outputs details of the shortest path.
    [dist, path, ~]=graphshortestpath(DistMat,Istart(1),Endpoints_ID(n(1)));
 
%% Get primary root from Unet 
% read Unet output
fname2 = strcat(fname,'.png');
%fname2(fname2=='-') = '_';
Unet_BW_raw = imread(fullfile(inputFolder2,'u-net',fname2));
Unet_BW_raw = imbinarize(Unet_BW_raw);
Unet_BW= remove_reconstruction(Unet_BW_raw);
SE = strel('disk',20);
Unet_BW=imdilate(Unet_BW,SE);

% identify skeleton point that overlap with UNet output
[rows,cols] = find(Unet_BW);
rootseg=[rows,cols];
% find how many nodule center matches with the co-ordinate
[C,ia,ib] = intersect(rootseg,Pt,'rows');
path2 = ib;
% consider that as primary root or concatenat results from shortest path
%figure;imshow(skeBW);hold on;
%scatter(Pt(path2,2),Pt(path2,1));
%scatter(Pt(path,2),Pt(path,1),'*r');

% Unet output
U = [Pt(path2,2),Pt(path2,1)];

%preallocats memory for replotting the root from x and y coordinates
[Row,Column] = size(Root);
new_primary = zeros(Row,Column);
%plots the connected components of the root which are in x and y coordinates
for n = 1 : max(size(U))
    new_primary(U(n,2),U(n,1))=1;
end

%skeletontize input image, remove spurious branches, then calculate length
skel=logical(new_primary);
%skel=bwmorph(skel,'spur',10);
B = bwmorph(skel, 'branchpoints');
E = bwmorph(skel, 'endpoints');
[ye,xe] = find(E);
B_loc = find(B);
Dmask = false(size(skel));
for k = 1:numel(xe)
    D = bwdistgeodesic(skel,xe(k),ye(k));
    distanceToBranchPt = min(D(B_loc));
    Dmask(D < distanceToBranchPt) =true;
end
skelD = skel - Dmask;
[y_new,x_new] = find(skelD);

U_new = [x_new,y_new];
%ARIA output
T = [Pt(path,2),Pt(path,1)];
Tx = T(T(:,2)>(max(y_new)),:);
Tx2 = T(T(:,2)<(min(y_new)),:);
U2 = [U_new;Tx;Tx2];

%figure;imshow(skeBW);hold on;
%scatter(x_unet,y_unet,'*g');
%scatter(U2(:,1),U2(:,2),'*r');
%hold off;
U2(:, [1 2]) = U2(:, [2 1]); 
%U2_clean = [y_new,x_new];
% find how many nodule center matches with the co-ordinate
[C2,ia2,ib2] = intersect(U2,Pt,'rows');
path_prim = ib2;

%figure;imshow(skeBW);hold on;
%scatter(Pt(path2,2),Pt(path2,1));
%scatter(Pt(path_prim,2),Pt(path_prim,1),'*g');
end